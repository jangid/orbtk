use crate::prelude::*;

property!(
    /// `Theme` describes the css theme of a window.
    Theme(ThemeValue)
);
