use crate::prelude::*;

property!(
    /// `Selected` describes the selected state of a widget.
    Selected(bool)
);
