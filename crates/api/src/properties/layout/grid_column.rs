use crate::prelude::*;

property!(
    /// `GridColumn` describes the column position of a widget on the `Grid`.
    GridColumn(usize)
);
